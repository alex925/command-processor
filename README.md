**Коммандный процессор**

Консольное приложение для управления набором целых чисел. 
Поддерживает расширение функциональности с помощью плагинов.

Возможен запуск в 2 режимах: интерактивный (вводится команда 
и она исполняется сразу же) и исполнение команд из скрипта.

При написании команд можно использовать экранированные последовательности: 
`\t - символ табуляции, \s - пробел, \\ - одиночный слеш`

Поддерживаемые команды:

`list [разделитель]` - выводит содержимое на экран

`clear` - удаляет все из набора 

`add value` - добавляет новое значение в набор

`del index [end_index]` - удаляет из набора заданный элемент или элементы
 в указанном диопазоне

`find value` - ищет значения в наборе

`set index value` - устанавливает значение в указанную позицию

`get index` - читает значение в указанной позиции 

`sort [asc|desc]` - сортирует набор чисел по возрастанию (asc) или убыванию (desc)

`unique` - удаляет дубликаты в наборе

`save имя_файла [разделитель]` - сохраняет набор в файл с заданным именем

`savetohtml имя_файла [разделитель]` - сохраняет набор в html файл с заданным именем

`load имя_файла [разделитель]` - загружает набор из текстового файла

`count` - отображает количество чисел в наборе

**Зависимости**

- Java 8
- Gradle

**Запуск**

- Выполнить refresh для всех gradle проектов (чтобы gradle скачал все 
необходимые зависимости)
- Установить расширения из gradle проекта :extension
- Запустить класс Main из модуля number-processor