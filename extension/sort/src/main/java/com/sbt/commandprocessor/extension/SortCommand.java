package com.sbt.commandprocessor.extension;

import com.sbt.commandprocessor.commands.Command;
import com.sbt.commandprocessor.datastorage.IDataStorage;
import com.sbt.commandprocessor.datastorage.SortingOrder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("sort")
public class SortCommand extends Command {

    public SortCommand() {
        defParams.add(
                new ParamMetaInfo("sortingOrder", "ASC", (String i) -> (SortingOrder.valueOf(i.toUpperCase())))
        );
        description = "Сортирует числа по возврастанию или убыванию.";
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) {
        dataStorage.sort((SortingOrder) params.get(0));
        return "";
    }
}