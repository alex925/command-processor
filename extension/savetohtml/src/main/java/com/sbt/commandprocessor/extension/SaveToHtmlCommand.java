package com.sbt.commandprocessor.extension;

import com.sbt.commandprocessor.commands.Command;
import com.sbt.commandprocessor.datastorage.FileType;
import com.sbt.commandprocessor.datastorage.IDataStorage;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("savetohtml")
public class SaveToHtmlCommand extends Command {

    public SaveToHtmlCommand() {
        defParams.add(new ParamMetaInfo("fileName", null, null));
        defParams.add(new ParamMetaInfo("separator", "\t", ParamMetaInfo::separatorConverter));
        description = "Сохраняет набор чисел в html файл.";
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) {
        dataStorage.saveToFile((String) params.get(0), (String) params.get(1), FileType.HTML);
        return "";
    }
}