package com.sbt.commandprocessor.datastorage.testingcommands;

import com.sbt.commandprocessor.commands.Command;
import com.sbt.commandprocessor.datastorage.IDataStorage;
import com.sbt.commandprocessor.exceptions.ErrorExecuteCommand;

import java.util.List;

public class DelCommand extends Command {

    public DelCommand() {
        defParams.add(new ParamMetaInfo("index", null, (String i) -> (new Integer(i))));
        defParams.add(new ParamMetaInfo("stopIndex", "-1", (String i) -> (new Integer(i))));
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) throws ErrorExecuteCommand {
        dataStorage.remove((int) params.get(0), (int) params.get(1));
        return "";
    }
}