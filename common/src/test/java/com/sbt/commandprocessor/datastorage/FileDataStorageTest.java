package com.sbt.commandprocessor.datastorage;

import com.sbt.commandprocessor.exceptions.ErrorExecuteCommand;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;

public class FileDataStorageTest {
    private FileDataStorage storage = spy(new FileDataStorage());

    @Before
    public void setUp() {
        storage.add(Arrays.asList(1L, 22L, 3L, 5L, 30L, 0L));
    }

    @Test
    public void loadFileTest() throws ErrorExecuteCommand, URISyntaxException {
        storage.clear();
        storage.loadFile(
                new File(
                        FileDataStorageTest.class.getClassLoader().getResource("data.txt").toURI()
                ).getPath(),
                "\n"
        );
        assertEquals(storage.getAll(), Arrays.asList(1L, 2L, 3L, 4L, 4L, 5L));
    }

    @Test(expected = ErrorExecuteCommand.class)
    public void loadFileNotValidPathTest() throws ErrorExecuteCommand {
        storage.clear();
        storage.loadFile("/someFile.txt", "\n");
    }

    @Test(expected = ErrorExecuteCommand.class)
    public void loadFileNotValidDataTest() throws ErrorExecuteCommand, URISyntaxException {
        storage.clear();
        storage.loadFile(
                new File(
                        FileDataStorageTest.class.getClassLoader().getResource("not_valid_data.txt").toURI()
                ).getPath(),
                "\n"
        );
    }

    @Test
    public void removeTest() throws ErrorExecuteCommand {
        storage.remove(1, 3);
        assertEquals(storage.getAll(), Arrays.asList(1L, 5L, 30L, 0L));
    }

    @Test
    public void getTest() throws ErrorExecuteCommand {
        Long num = storage.get(1);
        assertEquals(num,  new Long(22));
    }

    @Test
    public void findTest() {
        Integer num = storage.find(3L);
        assertEquals(num, new Integer(2));
    }

    @Test
    public void sortTest() {
        storage.sort(SortingOrder.ASC);
        assertEquals(storage.getAll(), Arrays.asList(0L, 1L, 3L, 5L, 22L, 30L));
        storage.sort(SortingOrder.DESC);
        assertEquals(storage.getAll(), Arrays.asList(30L, 22L, 5L, 3L, 1L, 0L));
    }

    @Test
    public void sortNullTest() {
        storage.sort(null);
        assertEquals(storage.getAll(), Arrays.asList(1L, 22L, 3L, 5L, 30L, 0L));
    }

    @Test
    public void removeDuplicatesTest() {
        storage.add(1L);
        storage.removeDuplicates();
        assertEquals(storage.getAll(), Arrays.asList(1L, 22L, 3L, 5L, 30L, 0L));
    }
}
