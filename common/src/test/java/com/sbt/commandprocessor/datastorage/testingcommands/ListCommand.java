package com.sbt.commandprocessor.datastorage.testingcommands;

import com.sbt.commandprocessor.commands.Command;
import com.sbt.commandprocessor.datastorage.IDataStorage;

import java.util.List;

public class ListCommand extends Command {

    public ListCommand() {
        defParams.add(new ParamMetaInfo("separator", "\t", null));
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) {
        return dataStorage.list((String) params.get(0));
    }
}

