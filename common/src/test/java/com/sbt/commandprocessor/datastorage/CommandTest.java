package com.sbt.commandprocessor.datastorage;

import com.sbt.commandprocessor.commands.Command;
import com.sbt.commandprocessor.datastorage.testingcommands.DelCommand;
import com.sbt.commandprocessor.datastorage.testingcommands.ListCommand;
import com.sbt.commandprocessor.exceptions.ErrorExecuteCommand;
import com.sbt.commandprocessor.exceptions.ErrorParsingParam;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CommandTest {

    private Command delCommand;
    private Command listCommand;
    private IDataStorage dataStorage;

    @Before
    public void setUp() {
        delCommand = new DelCommand();
        listCommand = new ListCommand();
        dataStorage = new FileDataStorage();
        dataStorage.add(Arrays.asList(1L, 2L, 3L, 4L, 5L));
    }

    @Test
    public void delByIndexTest() throws ErrorParsingParam, ErrorExecuteCommand {
        List<Object> params = delCommand.prepareParams(Arrays.asList("0"));
        delCommand.run(params, dataStorage);
        assertEquals(dataStorage.getAll(), Arrays.asList(2L, 3L, 4L, 5L));
    }

    @Test
    public void delRangeTest() throws ErrorParsingParam, ErrorExecuteCommand {
        List<Object> params = delCommand.prepareParams(Arrays.asList("0", "2"));
        delCommand.run(params, dataStorage);
        assertEquals(dataStorage.getAll(), Arrays.asList(3L, 4L, 5L));
    }

    @Test
    public void listTest() throws ErrorParsingParam, ErrorExecuteCommand {
        List<Object> params = listCommand.prepareParams(Arrays.asList("-"));
        String result = listCommand.run(params, dataStorage);
        assertEquals(result, "1-2-3-4-5");
    }

    @Test
    public void listDefaultSepTest() throws ErrorParsingParam, ErrorExecuteCommand {
        List<Object> params = listCommand.prepareParams(Arrays.asList());
        String result = listCommand.run(params, dataStorage);
        assertEquals(result, "1\t2\t3\t4\t5");
    }
}
