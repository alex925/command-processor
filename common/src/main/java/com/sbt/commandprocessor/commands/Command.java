package com.sbt.commandprocessor.commands;

import com.sbt.commandprocessor.datastorage.IDataStorage;
import com.sbt.commandprocessor.exceptions.ErrorExecuteCommand;
import com.sbt.commandprocessor.exceptions.ErrorParsingParam;

import java.util.*;

/**
 * Базовый класс для всех команнд. Позволяет объявлять аргументы команд
 * и указывать как парсить и преобразовывать их аргументы.
 */
public abstract class Command {

    // Содержит описание аргументов команды и их метаинформацию
    protected List<ParamMetaInfo> defParams = new LinkedList<>();
    protected String description = "";

    public Command() {}

    public abstract String run(List<Object> params, IDataStorage dataStorage) throws ErrorExecuteCommand;

    /**
     * Обрабатывает параметры конмады (подставляет значения по умолчанию,
     * конвертирует в нужные типы)
     * @param params параметры в сыром виде
     * @return подготовленные для дальнейшей работы параметры
     * @throws ErrorParsingParam
     */
    public List<Object> prepareParams(List<String> params) throws ErrorParsingParam {
        List<Object> parsedParams = new ArrayList<>();
        ParamMetaInfo paramMetaInfo;
        String rawParam;
        for (int i = 0; i < defParams.size(); i++) {
            paramMetaInfo = defParams.get(i);
            try {
                rawParam = params.get(i);
            } catch (IndexOutOfBoundsException e) {
                rawParam = getDefaultValue(paramMetaInfo);
            }

            Object param = convertParam(rawParam, paramMetaInfo);
            parsedParams.add(param);
        }
        return parsedParams;
    }

    private String getDefaultValue(ParamMetaInfo paramMetaInfo) throws ErrorParsingParam {
        if (paramMetaInfo.defaultValue == null) {
            throw new ErrorParsingParam(String.format("Отсутствует обязательный параметр <%s>", paramMetaInfo.name));
        }
        return paramMetaInfo.defaultValue;
    }

    private Object convertParam(String rawParam, ParamMetaInfo paramMetaInfo) throws ErrorParsingParam {
        try {
            return paramMetaInfo.converter.covert(rawParam);
        } catch (Exception e) {
            throw new ErrorParsingParam(
                    String.format("Ошибка при преобразовании <%s>. Проверьте правильность ввода.", rawParam)
            );
        }
    }

    public int getCountParams() {
        return defParams.size();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    static public class ParamMetaInfo {

        private String name;
        private String defaultValue;
        private Converter converter;
        private static Map<String, String> unescapeSequence = new HashMap<>();
        static {
            unescapeSequence.put("\\t", "\t");
            unescapeSequence.put("\\b", "\b");
            unescapeSequence.put("\\n", "\n");
            unescapeSequence.put("\\r", "\r");
            unescapeSequence.put("\\f", "\f");
            unescapeSequence.put("\\s", " ");
            unescapeSequence.put("\\\\", "\\");
        }

        public ParamMetaInfo(String name, String defaultValue, Converter converter) {
            this.name = name;
            this.defaultValue = defaultValue;

            if (converter == null) {
                this.converter = ParamMetaInfo::defaultConverter;
            } else {
                this.converter = converter;
            }
        }

        private static String defaultConverter(String value) {
            return value;
        }

        public static String separatorConverter(String value) {
            String converted = unescapeSequence.get(value);
            if (converted == null) {
                return value;
            }
            return converted;
        }

        public String getDefaultValue() {
            return defaultValue;
        }

        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        public Converter getConverter() {
            return converter;
        }

        public void setConverter(Converter converter) {
            this.converter = converter;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
