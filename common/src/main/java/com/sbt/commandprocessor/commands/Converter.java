package com.sbt.commandprocessor.commands;

@FunctionalInterface
public interface Converter {
    Object covert(String i);
}
