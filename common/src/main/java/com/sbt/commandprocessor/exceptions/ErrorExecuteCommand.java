package com.sbt.commandprocessor.exceptions;

public class ErrorExecuteCommand extends Exception {
    public ErrorExecuteCommand() {
        super();
    }

    public ErrorExecuteCommand(String message) {
        super(message);
    }

    public ErrorExecuteCommand(String message, Throwable cause) {
        super(message, cause);
    }

    public ErrorExecuteCommand(Throwable cause) {
        super(cause);
    }
}
