package com.sbt.commandprocessor.exceptions;

public class ErrorParsingParam extends Exception {
    public ErrorParsingParam() {
        super();
    }

    public ErrorParsingParam(String message) {
        super(message);
    }

    public ErrorParsingParam(String message, Throwable cause) {
        super(message, cause);
    }

    public ErrorParsingParam(Throwable cause) {
        super(cause);
    }
}
