package com.sbt.commandprocessor.datastorage;

import com.sbt.commandprocessor.exceptions.ErrorExecuteCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

public class FileDataStorage implements IDataStorage {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileDataStorage.class);
    private List<Long> numbers = new ArrayList<>();

    public FileDataStorage() {}

    public FileDataStorage(String filePath, String separator) throws ErrorExecuteCommand {
        this.loadFile(filePath, separator);
    }

    @Override
    public void loadFile(String filePath, String separator) throws ErrorExecuteCommand {
        if (filePath == null || "".equals(filePath)) {
            String message = "Ошибка при попытке инициализировать хранилище из файла. " +
                    "Не указан путь к файлу.";
            LOGGER.error(message);
            throw new ErrorExecuteCommand(message);
        }
        if (!Paths.get(filePath).toFile().exists()) {
            String message = "Ошибка при попытке инициализировать хранилище из файла. " +
                    "Указан не существующий путь.";
            LOGGER.error(message);
            throw new ErrorExecuteCommand(message);
        }

        String fileText = "";
        try {
            fileText = new String(Files.readAllBytes(Paths.get(filePath)), StandardCharsets.UTF_8);
        } catch (IOException e) {
            String message = "Ошибка при попытке открыть файл. Проверьте, что файл сущестует.";
            LOGGER.error(message, e);
            throw new ErrorExecuteCommand(message);
        }

        for (String i : fileText.split(separator)) {
            try {
                numbers.add(new Long(i.trim()));
            } catch (Exception e) {
                String message = "Ошибка при обработке файла";
                LOGGER.error(message);
                throw new ErrorExecuteCommand(message, e);
            }
        }
    }

    @Override
    public void saveToFile(String filePath, String separator, FileType fileType) {
        try(OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(filePath), "UTF-8")) {
            if (FileType.TXT == fileType) {
                writer.write(numbersToTxt(separator));
            } else if (FileType.HTML == fileType) {
                writer.write(numbersToHtml());
            }
            writer.flush();
        } catch(IOException e) {
            LOGGER.error("Возникла ошибка при попытке сохранить данные в файл", e);
        }
    }

    private String numbersToTxt(String separator) {
        return numbers.stream().map(String::valueOf).collect(Collectors.joining(separator));
    }

    private String numbersToHtml() {
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Командный менеджер - Выргрузка</title>\n" +
                "</head>\n" +
                "<body>\n" +
                numbersToTxt(", ") + "\n" +
                "</body>\n" +
                "</html>";
    }

    @Override
    public String list(String separator) {
        return numbers.stream().map(String::valueOf).collect(Collectors.joining(separator));
    }

    @Override
    public void add(Long num) {
        numbers.add(num);
    }

    @Override
    public void set(int index, Long num) throws ErrorExecuteCommand {
        try {
            numbers.set(index, num);
        } catch (IndexOutOfBoundsException e) {
            throw new ErrorExecuteCommand("Вы пытаетесь присвоить значение элементу, который ещё не существует");
        }
    }

    @Override
    public void add(List<Long> nums) {
        numbers.addAll(nums);
    }

    @Override
    public Long get(int index) throws ErrorExecuteCommand {
        try {
            return numbers.get(index);
        } catch (IndexOutOfBoundsException e) {
            throw new ErrorExecuteCommand("Вы пытаетесь обратиться к элементу, который ещё не существует");
        }
    }

    @Override
    public void remove(int index, int stopIndex) throws ErrorExecuteCommand {
        try {
            if (stopIndex == -1) {
                numbers.remove(index);
            } else {
                numbers.subList(index, stopIndex).clear();
            }
        } catch (IndexOutOfBoundsException e) {
            throw new ErrorExecuteCommand("Вы пытаетесь удалить не сущесвующий элемент(ы)");
        }
    }

    @Override
    public int find(Long num) {
        return numbers.indexOf(num);
    }

    @Override
    public void sort(SortingOrder sortingOrder) {
        if (SortingOrder.ASC == sortingOrder) {
            Collections.sort(numbers);
        } else if (SortingOrder.DESC == sortingOrder) {
            Collections.sort(numbers, Collections.reverseOrder());
        }
    }

    @Override
    public void removeDuplicates() {
        numbers = new ArrayList<>(new LinkedHashSet<Long>(numbers));
    }

    @Override
    public int getCount() {
        return numbers.size();
    }

    @Override
    public List<Long> getAll() {
        return numbers;
    }

    @Override
    public void clear() {
        numbers.clear();
    }

    @Override
    public String toString() {
        return "FileDataStorage<" + numbers.stream().map(String::valueOf).collect(Collectors.joining(", ")) + ">";
    }
}
