package com.sbt.commandprocessor.datastorage;

import com.sbt.commandprocessor.exceptions.ErrorExecuteCommand;

import java.util.List;

public interface IDataStorage {
    void loadFile(String filePath, String separator) throws ErrorExecuteCommand;
    void saveToFile(String filePath, String separator, FileType fileType);
    String list(String separator);
    void add(Long num);
    void add(List<Long> nums);
    void set(int index, Long num) throws ErrorExecuteCommand;
    Long get(int index) throws ErrorExecuteCommand;
    void remove(int index, int endIndex) throws ErrorExecuteCommand;
    int find(Long num);
    void sort(SortingOrder sortingOrder);
    void removeDuplicates();
    int getCount();
    List<Long> getAll();
    void clear();
}
