package com.sbt.commandprocessor.datastorage;

public enum FileType {
    TXT, HTML
}
