package com.sbt.commandprocessor.datastorage;

public enum SortingOrder {
    ASC, DESC
}
