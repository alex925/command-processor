package com.sbt.commandprocessor.commands;

import com.sbt.commandprocessor.datastorage.FileDataStorage;
import com.sbt.commandprocessor.datastorage.IDataStorage;
import com.sbt.commandprocessor.exceptions.ErrorExecuteCommand;
import com.sbt.commandprocessor.exceptions.ErrorParsingParam;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestCommands {

    private Command addCommand = new AddCommand();
    private Command clearCommand = new ClearCommand();
    private Command countCommand = new CountCommand();
    private Command delCommand = new DelCommand();
    private Command findCommand = new FindCommand();
    private Command getCommand = new GetCommand();
    private Command listCommand = new ListCommand();
    private Command loadCommand = new LoadCommand();
    private Command saveCommand = new SaveCommand();
    private Command setCommand = new SetCommand();
    private Command uniqueCommand = new UniqueCommand();

    private IDataStorage dataStorage = new FileDataStorage();

    @Before
    public void setUp() {
        dataStorage.add(Arrays.asList(1L, 2L, 3L, 4L));
    }

    @Test
    public void addTest() throws ErrorParsingParam, ErrorExecuteCommand {
        List<Object> params = addCommand.prepareParams(Arrays.asList("0"));
        addCommand.run(params, dataStorage);
        assertEquals(dataStorage.getAll(), Arrays.asList(1L, 2L, 3L, 4L, 0L));
    }

    @Test
    public void clearTest() throws ErrorParsingParam, ErrorExecuteCommand {
        List<Object> params = clearCommand.prepareParams(Arrays.asList());
        clearCommand.run(params, dataStorage);
        assertEquals(dataStorage.getAll(), Arrays.asList());
    }

    @Test
    public void countTest() throws ErrorParsingParam, ErrorExecuteCommand {
        List<Object> params = countCommand.prepareParams(Arrays.asList("0"));
        countCommand.run(params, dataStorage);
        assertEquals(dataStorage.getCount(), 4);
    }

    @Test
    public void delTest() throws ErrorParsingParam, ErrorExecuteCommand {
        List<Object> params = delCommand.prepareParams(Arrays.asList("0"));
        delCommand.run(params, dataStorage);
        assertEquals(dataStorage.getAll(), Arrays.asList(2L, 3L, 4L));
    }

    @Test
    public void delRangeTest() throws ErrorParsingParam, ErrorExecuteCommand {
        List<Object> params = delCommand.prepareParams(Arrays.asList("0", "2"));
        delCommand.run(params, dataStorage);
        assertEquals(dataStorage.getAll(), Arrays.asList(3L, 4L));
    }

    @Test
    public void findTest() throws ErrorParsingParam, ErrorExecuteCommand {
        List<Object> params = findCommand.prepareParams(Arrays.asList("4"));
        String out = findCommand.run(params, dataStorage);
        assertEquals(out, "Значение 4 найдено в позиции 3");

        params = findCommand.prepareParams(Arrays.asList("343"));
        out = findCommand.run(params, dataStorage);
        assertEquals(out, "Значение 343 не найдено");
    }

    @Test
    public void getTest() throws ErrorParsingParam, ErrorExecuteCommand {
        List<Object> params = getCommand.prepareParams(Arrays.asList("0"));
        String value = getCommand.run(params, dataStorage);
        assertEquals(value, "1");
    }

    @Test(expected = ErrorExecuteCommand.class)
    public void getNegativeTest() throws ErrorParsingParam, ErrorExecuteCommand {
        List<Object> params = getCommand.prepareParams(Arrays.asList("-10"));
        String value = getCommand.run(params, dataStorage);
    }

    @Test
    public void listTest() throws ErrorParsingParam, ErrorExecuteCommand {
        List<Object> params = listCommand.prepareParams(Arrays.asList("-"));
        String result = listCommand.run(params, dataStorage);
        assertEquals(result, "1-2-3-4");
    }

    @Test
    public void listDefaultSepTest() throws ErrorParsingParam, ErrorExecuteCommand {
        List<Object> params = listCommand.prepareParams(Arrays.asList());
        String result = listCommand.run(params, dataStorage);
        assertEquals(result, "1\t2\t3\t4");
    }

    @Test
    public void loadTest() throws ErrorParsingParam, ErrorExecuteCommand, URISyntaxException {
        dataStorage.clear();
        List<Object> params = loadCommand.prepareParams(
                Arrays.asList(
                        new File(TestCommands.class.getClassLoader().getResource("load.txt").toURI()).getPath(),
                        "\\n"
                )
        );
        loadCommand.run(params, dataStorage);
        assertEquals(dataStorage.getAll(), Arrays.asList(1L, -1L, 45L, 2326L, 23L));
    }

    @Test(expected = ErrorExecuteCommand.class)
    public void loadNegativeTest() throws ErrorParsingParam, ErrorExecuteCommand, URISyntaxException {
        dataStorage.clear();
        List<Object> params = loadCommand.prepareParams(
                Arrays.asList(
                        new File(TestCommands.class.getClassLoader().getResource("load.txt").toURI()).getPath(),
                        "\\n-"
                )
        );
        loadCommand.run(params, dataStorage);
        assertEquals(dataStorage.getAll(), Arrays.asList(1L, -1L, 45L, 2326L, 23L));
    }

    @Test
    public void saveTest() throws Exception {
        File outFile = File.createTempFile("command_processor", ".txt");
        outFile.deleteOnExit();
        List<Object> params = saveCommand.prepareParams(Arrays.asList(outFile.getPath(), "\n"));
        saveCommand.run(params, dataStorage);
        assertEquals(
                new String(Files.readAllBytes(Paths.get(outFile.getPath()))),
                new String(Files.readAllBytes(Paths.get(TestCommands.class.getClassLoader().getResource("save.txt").getPath())))
        );
    }

    @Test
    public void setTest() throws Exception {
        List<Object> params = setCommand.prepareParams(Arrays.asList("0", "-1111"));
        setCommand.run(params, dataStorage);
        assertEquals(dataStorage.get(0), new Long(-1111));
    }

    @Test(expected = ErrorExecuteCommand.class)
    public void setNegativeTest() throws Exception {
        List<Object> params = setCommand.prepareParams(Arrays.asList("10", "-1111"));
        setCommand.run(params, dataStorage);
    }

    @Test
    public void uniqueTest() throws Exception {
        dataStorage.add(1L);
        List<Object> params = uniqueCommand.prepareParams(Arrays.asList());
        uniqueCommand.run(params, dataStorage);
        assertEquals(dataStorage.getAll(), Arrays.asList(1L, 2L, 3L, 4L));
    }
}
