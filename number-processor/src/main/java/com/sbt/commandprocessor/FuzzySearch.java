package com.sbt.commandprocessor;

import java.util.ArrayList;
import java.util.List;

public class FuzzySearch {

    private static int maxDistance = 2;

    /**
     * Рачитывает расстояние Левенштейна
     * @param sequence1 строка 1
     * @param sequence2 строка 2
     * @return расчитанное растояние
     */
    private static int computeLevenshteinDistance(CharSequence sequence1, CharSequence sequence2) {
        int len0 = sequence1.length() + 1;
        int len1 = sequence2.length() + 1;

        int[] cost = new int[len0];
        int[] newcost = new int[len0];

        for (int i = 0; i < len0; i++) {
            cost[i] = i;
        }

        for (int j = 1; j < len1; j++) {
            newcost[0] = j;

            for(int i = 1; i < len0; i++) {
                int match = (sequence1.charAt(i - 1) == sequence2.charAt(j - 1)) ? 0 : 1;

                int cost_replace = cost[i - 1] + match;
                int cost_insert  = cost[i] + 1;
                int cost_delete  = newcost[i - 1] + 1;

                newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
            }

            int[] swap = cost;
            cost = newcost;
            newcost = swap;
        }

        return cost[len0 - 1];
    }

    /**
     * Ищет в dict слова похожие на userInput
     * @param dict словарь в котором производится поиск
     * @param userInput слово для которого подбираются похожие слова
     * @return список похожих слов
     */
    public static List<String> searchSimilar(List<String> dict, String userInput) {
        List<Integer> computedDiff = new ArrayList<>();
        for (String i: dict) {
            computedDiff.add(computeLevenshteinDistance(i, userInput));
        }

        List<String> similarCommands = new ArrayList<>();
        for (int i = 0; i < computedDiff.size(); i++) {
            if (computedDiff.get(i) <= maxDistance) {
                similarCommands.add(dict.get(i));
            }
        }

        return similarCommands;
    }

    public static int getMaxDistance() {
        return maxDistance;
    }

    public static void setMaxDistance(int maxDistance) {
        FuzzySearch.maxDistance = maxDistance;
    }
}