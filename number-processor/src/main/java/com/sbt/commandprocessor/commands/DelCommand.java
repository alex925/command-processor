package com.sbt.commandprocessor.commands;

import com.sbt.commandprocessor.datastorage.IDataStorage;
import com.sbt.commandprocessor.exceptions.ErrorExecuteCommand;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("del")
public class DelCommand extends Command {

    public DelCommand() {
        defParams.add(new ParamMetaInfo("index", null, (String i) -> (new Integer(i))));
        defParams.add(new ParamMetaInfo("stopIndex", "-1", (String i) -> (new Integer(i))));
        description = "Удаляет из набора элемент с заданным индексом или в заданом диопазоне.";
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) throws ErrorExecuteCommand {
        dataStorage.remove((int) params.get(0), (int) params.get(1));
        return "";
    }
}
