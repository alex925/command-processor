package com.sbt.commandprocessor.commands;

import com.sbt.commandprocessor.datastorage.IDataStorage;
import org.springframework.stereotype.Component;

import java.util.List;


@Component("count")
public class CountCommand extends Command {

    public CountCommand() {
        description = "Выводит количество чисел в наборе.";
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) {
        return String.valueOf(dataStorage.getCount());
    }
}
