package com.sbt.commandprocessor.commands;

import com.sbt.commandprocessor.datastorage.IDataStorage;
import com.sbt.commandprocessor.exceptions.ErrorExecuteCommand;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("load")
public class LoadCommand extends Command {

    public LoadCommand() {
        defParams.add(new ParamMetaInfo("fileName", null, null));
        defParams.add(new ParamMetaInfo("separator", "\t", ParamMetaInfo::separatorConverter));
        description = "Загружает набор чисел из текстового файла.";
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) throws ErrorExecuteCommand {
        dataStorage.loadFile((String) params.get(0), (String) params.get(1));
        return "";
    }
}
