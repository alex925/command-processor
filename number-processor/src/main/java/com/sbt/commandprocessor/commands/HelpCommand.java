package com.sbt.commandprocessor.commands;

import com.sbt.commandprocessor.datastorage.IDataStorage;
import com.sbt.commandprocessor.spring.ApplicationContextProvider;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component("help")
public class HelpCommand extends Command {

    private Map<String, ? extends Command> commands = ApplicationContextProvider.AppContext.getApplicationContext().getBeansOfType(Command.class);

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) {
        StringBuilder help = new StringBuilder();
        for (Map.Entry<String, ? extends Command> i : commands.entrySet()) {
            help.append(i.getKey());
            help.append(" - ");
            help.append(i.getValue().getDescription());
            help.append("\n");
        }
        return help.toString().trim();
    }
}
