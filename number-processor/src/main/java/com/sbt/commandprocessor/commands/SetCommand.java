package com.sbt.commandprocessor.commands;

import com.sbt.commandprocessor.datastorage.IDataStorage;
import com.sbt.commandprocessor.exceptions.ErrorExecuteCommand;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("set")
public class SetCommand extends Command {

    public SetCommand() {
        defParams.add(new ParamMetaInfo("index", null, Integer::valueOf));
        defParams.add(new ParamMetaInfo("value", null, Long::valueOf));
        description = "Устанавливает значение в указанную позицию.";
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) throws ErrorExecuteCommand {
        dataStorage.set((int) params.get(0), (Long) params.get(1));
        return "";
    }
}
