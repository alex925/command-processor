package com.sbt.commandprocessor.commands;

import com.sbt.commandprocessor.datastorage.IDataStorage;
import org.springframework.stereotype.Component;

import java.util.List;


@Component("exit")
public class ExitCommand extends Command {

    public ExitCommand() {
        description = "Завершает работу приложения";
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) {
        System.exit(0);
        return "";
    }
}
