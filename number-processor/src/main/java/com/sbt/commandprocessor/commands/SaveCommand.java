package com.sbt.commandprocessor.commands;

import com.sbt.commandprocessor.datastorage.FileType;
import com.sbt.commandprocessor.datastorage.IDataStorage;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("save")
public class SaveCommand extends Command {

    public SaveCommand() {
        defParams.add(new ParamMetaInfo("fileName", null, null));
        defParams.add(new ParamMetaInfo("separator", "\t", ParamMetaInfo::separatorConverter));
        description = "Сохраняет набор чисел в текстовый файл.";
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) {
        dataStorage.saveToFile((String) params.get(0), (String) params.get(1), FileType.TXT);
        return "";
    }
}