package com.sbt.commandprocessor.commands;

import com.sbt.commandprocessor.datastorage.IDataStorage;
import com.sbt.commandprocessor.exceptions.ErrorExecuteCommand;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("unique")
public class UniqueCommand extends Command {

    public UniqueCommand() {
        description = "Удаляет повторяющиеся элементы.";
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) throws ErrorExecuteCommand {
        dataStorage.removeDuplicates();
        return "";
    }
}
