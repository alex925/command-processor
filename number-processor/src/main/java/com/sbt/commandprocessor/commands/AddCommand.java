package com.sbt.commandprocessor.commands;

import com.sbt.commandprocessor.datastorage.IDataStorage;
import org.springframework.stereotype.Component;

import java.util.List;


@Component("add")
public class AddCommand extends Command {

    public AddCommand() {
        defParams.add(new ParamMetaInfo("value", null, (String i) -> (new Long(i))));
        description = "Добавляет в набор новое число";
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) {
        dataStorage.add((Long) params.get(0));
        return "";
    }
}
