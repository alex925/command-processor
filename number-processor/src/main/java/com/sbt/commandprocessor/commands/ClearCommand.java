package com.sbt.commandprocessor.commands;

import com.sbt.commandprocessor.datastorage.IDataStorage;
import org.springframework.stereotype.Component;

import java.util.List;


@Component("clear")
public class ClearCommand extends Command {

    public ClearCommand() {
        description = "Удляет все из набора";
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) {
        dataStorage.clear();
        return "";
    }
}
