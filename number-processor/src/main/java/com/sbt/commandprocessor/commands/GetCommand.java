package com.sbt.commandprocessor.commands;

import com.sbt.commandprocessor.datastorage.IDataStorage;
import com.sbt.commandprocessor.exceptions.ErrorExecuteCommand;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("get")
public class GetCommand extends Command {

    public GetCommand() {
        defParams.add(new ParamMetaInfo("index", null, Integer::valueOf));
        description = "Извлекает значение из указанной позиции.";
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) throws ErrorExecuteCommand {
        return dataStorage.get((int) params.get(0)).toString();
    }
}
