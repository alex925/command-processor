package com.sbt.commandprocessor.commands;

import com.sbt.commandprocessor.datastorage.IDataStorage;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("list")
public class ListCommand extends Command {

    public ListCommand() {
        defParams.add(new ParamMetaInfo("separator", "\t", ParamMetaInfo::separatorConverter));
        description = "Выводит список добавленных чисел";
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) {
        return dataStorage.list((String) params.get(0));
    }
}
