package com.sbt.commandprocessor.commands;

import com.sbt.commandprocessor.datastorage.IDataStorage;
import org.springframework.stereotype.Component;

import java.util.List;


@Component("find")
public class FindCommand extends Command {

    public FindCommand() {
        defParams.add(new ParamMetaInfo("value", null, (String i) -> (new Long(i))));
        description = "Прозводит поиск в наборе";
    }

    @Override
    public String run(List<Object> params, IDataStorage dataStorage) {
        int index = dataStorage.find((Long) params.get(0));
        if (index == -1) {
            return String.format("Значение %d не найдено", (Long) params.get(0));
        }
        return String.format("Значение %d найдено в позиции %d", (Long) params.get(0), index);
    }
}
