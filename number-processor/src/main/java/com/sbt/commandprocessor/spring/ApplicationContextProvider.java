package com.sbt.commandprocessor.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class ApplicationContextProvider implements ApplicationContextAware {

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        AppContext.setApplicationContext(applicationContext);
    }

    public static class AppContext {
        private static ApplicationContext applicationContext;

        private AppContext() {}

        public static ApplicationContext getApplicationContext() {
            return applicationContext;
        }

        public static void setApplicationContext(ApplicationContext applicationContext) {
            AppContext.applicationContext = applicationContext;
        }
    }
}
