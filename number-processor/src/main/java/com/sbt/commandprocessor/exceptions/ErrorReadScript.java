package com.sbt.commandprocessor.exceptions;

public class ErrorReadScript extends Exception {
    public ErrorReadScript() {
        super();
    }

    public ErrorReadScript(String message) {
        super(message);
    }

    public ErrorReadScript(String message, Throwable cause) {
        super(message, cause);
    }

    public ErrorReadScript(Throwable cause) {
        super(cause);
    }
}
