package com.sbt.commandprocessor;

import com.sbt.commandprocessor.commands.Command;
import com.sbt.commandprocessor.datastorage.FileDataStorage;
import com.sbt.commandprocessor.datastorage.IDataStorage;
import com.sbt.commandprocessor.exceptions.ErrorExecuteCommand;
import com.sbt.commandprocessor.exceptions.ErrorParsingParam;
import com.sbt.commandprocessor.exceptions.ErrorReadScript;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static com.sbt.commandprocessor.spring.ApplicationContextProvider.AppContext;

public class NumberProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(NumberProcessor.class);
    private Map<String, ? extends Command> commands = AppContext.getApplicationContext().getBeansOfType(Command.class);
    private IDataStorage dataStorage = new FileDataStorage();

    public void executeScript(String scriptPath) {
        String[] script = new String[0];
        try {
            script = readScript(scriptPath);
        } catch (ErrorReadScript e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }

        String commandOutput = "";
        for (String i : script) {
            try {
                commandOutput = executeCommand(i);
            } catch (IllegalArgumentException | ErrorExecuteCommand | ErrorParsingParam e) {
                System.out.println(String.format("%s: <%s>", e.getMessage(), i));
                System.exit(1);
            } catch (Exception e) {
                String message = "Невозможно выполнить команду: " + i;
                LOGGER.error(message, e);
                System.out.println(message);
                System.exit(1);
            }

            if (!commandOutput.equals("")) {
                System.out.println(commandOutput);
            }
        }
    }

    private String[] readScript(String scriptPath) throws ErrorReadScript {
        String[] script;
        try {
            script = new String(Files.readAllBytes(Paths.get(scriptPath)), StandardCharsets.UTF_8).split("\n");
        } catch (IOException e) {
            String message = "Ошибка при чтении файла со скриптом. Проверьте существование файла и что " +
                    "используется кодировка UTF-8.";
            LOGGER.error(message);
            throw new ErrorReadScript(message);
        }
        return script;
    }

    public void runConsoleApp() {
        BufferedReader sysConsole = new BufferedReader(new InputStreamReader(System.in));
        String commandResult = "";
        String userInput = "";

        System.out.println("Выполните команду help для получения списка доступных команд");
        while (true) {
            System.out.print(">> ");

            try {
                userInput = sysConsole.readLine();
                commandResult = executeCommand(userInput);
            } catch (IllegalArgumentException | ErrorParsingParam | ErrorExecuteCommand e) {
                System.out.println(e.getMessage());
                continue;
            } catch (IOException e) {
                String message = "Ошибка при запуске приложения, приложение будет закрыто";
                LOGGER.error(message, e);
                System.out.println(message);
                System.exit(1);
            } catch (Exception e) {
                String message = "Невозможно выполнить команду";
                LOGGER.error(message, e);
                System.out.println(message);
                continue;
            }

            if (!commandResult.equals("")) {
                System.out.println(commandResult);
            }
        }
    }

    private String executeCommand(String rawInput) throws ErrorParsingParam, ErrorExecuteCommand {
        List<Object> parsedCommand = parseUserInput(rawInput);
        return commands.get(parsedCommand.get(0)).run((List) parsedCommand.get(1), dataStorage);
    }

    private List<Object> parseUserInput(String input) throws ErrorParsingParam {
        List<String> data = Arrays.asList(input.trim().split("\\s"));
        data.set(0, data.get(0).toLowerCase());

        Command command = commands.get(data.get(0));
        if (command == null) {
            String message;
            List<String> similarCommand = FuzzySearch.searchSimilar(new ArrayList<>(commands.keySet()), data.get(0));
            if (similarCommand.size() != 0) {
                message = String.format(
                        "Комманда '%s' отсуствует, возможно вы имели ввиду %s", data.get(0),
                        String.join(", ", similarCommand)
                );
            } else {
                message = String.format("Комманда '%s' отсуствует", data.get(0));
            }
            throw new IllegalArgumentException(message);
        }

        if ((data.size() - 1) > command.getCountParams()) {
            throw new IllegalArgumentException(
                    String.format(
                            "Комманда '%s' принимает %d параметров, получено %d", data.get(0), command.getCountParams(),
                            data.size() - 1)
            );
        }
        List<Object> parsedCommand = new ArrayList<>();
        parsedCommand.add(data.get(0));
        parsedCommand.add(command.prepareParams(data.subList(1, data.size())));
        return parsedCommand;
    }
}
