package com.sbt.commandprocessor;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		new ClassPathXmlApplicationContext("/context.xml");
		NumberProcessor numberProcessor = new NumberProcessor();
		if (args.length > 0) {
			numberProcessor.executeScript(args[0]);
		} else {
			numberProcessor.runConsoleApp();
		}
	}
}